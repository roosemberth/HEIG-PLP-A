Lab 1

## 1. Expressions

### A

> 'Z' < 'a'

True because in ascii order. Capital letters come before lowercase characters.

### B

> "abc" <= "ab"

This expression is reductible to `['z']` <= `[]`. This is false since a list
with an element is not smaller nor equal to an empty list.

### C

> if 2 < 3 then 3

This expression is not valid. The case where 2 is bigger than 3 (even if
mathematically impossible) cannot be proved by the compilier; hence type error.

### D

> [1,3,3,6] < [1,3,4]

True, same argument as A.

### E

> 4. + 3.5

The expression cannot be evaluated, because the compiler cannot infer the
meaning of `.`. The expression may be rewritten as `4.0 + 3.5`.

### F

> 5.0 - 4.2 / 2.1

`3.0`. Since `/` takes precedence. The fractional version of `/` will be used
which results in `2.0`. `5.0 - 2.0` = `3.0`.

### G

> if 4 > 4.5 then 3.0 else 'a'

This expresion does not typecheck since the type of the expression cannot be
unified.

### H

> 3 > 4 || 5 < 6 && not (7 /= 8)

False. The expression associates as following:
`((3 > 4) || (5 < 6)) && not (7 /= 8)`.

### I

> if 6 < 10 then 6.0 else 10.0

6.0

## 2. Listes

### A

> [1,2,3] !! ([1,2,3] !! 1)

3.

### B

> head []

Exception since the head of an empty list is not defined.

### C

> tail [1,2,3]

`[2,3]`.

### D

> "a":["b", "c"]

`["a", "b", "c"]`

### E

> "abc" ++ "d"

`"abcd"`

### F

> tail "abc" ++ "d"

`"bcd"`

### G

> [1] : [2,3,4]

Type error. A list of numbers cannot added to a list of numbers. It should be a
number.

### H

> ([1,2,3] !! 2 : []) ++ [3,4]

[3,3,4]

### I

> [3,2] ++ [1,2,3] !! head [1,2] : []

`[3,2,2]`. The expression associates as following:
`([3,2]) ++ (([1,2,3] !! (head [1,2])) : [])`.

## 3. Listes

### A

> [ x | x <- [1..20], odd x ]

`[1,3,5,7,9,11,13,15,17,19]`.

### B

> [ x | x <- [1..20], even x ]
> [2, 4..20]

`[2,4,6,8,10,12,14,16,18,20]`

### C

> [ x*x | x <- [1..100], x*x < 100 ]

`[1,4,9,16,25,36,49,64,81]`

### D

> [ (x,y) | x <- [1..7], y <- [1..7], x < 7, y < 7, x < y ]

`[(1,2),(1,3),(1,4),(1,5),(1,6),(2,3),(2,4),(2,5),(2,6),(3,4),(3,5),(3,6),(4,5),(4,6),(5,6)]`

### E

> let ub = 7; l = [1..ub]; in [ (x,y) | x <- l, y <- l, x < ub, y < ub, x < y ]

`[(1,2),(1,3),(1,4),(1,5),(1,6),(2,3),(2,4),(2,5),(2,6),(3,4),(3,5),(3,6),(4,5),(4,6),(5,6)]`

### F

> pred3c a b c = a*a + b*b == c*c
> let l = [1..10] in [ (x, y, z) |  x <- l, y <- l, z <- l, pred3c x y z ]

`[(3,4,5),(4,3,5),(6,8,10),(8,6,10)]`

## 4. Fonctions

### A

> somme x y z = x + y + z

`somme :: Num a => a -> a -> a -> a`

### B

> carré x = x * x

`carré :: Num a => a -> a`

### C

> pred4c a b c = carré a + carré b == carré c
> triangles n = let l = [1..10] in [ (x, y, z) |  x <- l, y <- l, z <- l, pred4c x y z ]

### D

> ou True _ = True
> ou _ True = True
> ou _ _ = False

### E

> et False _ = False
> et _ False = False
> et _ _ = True

### F

> mkTruthTable fn :: (Bool -> Bool -> c) -> [(Bool, Bool, c)]
> mkTruthTable fn = [ (x, y, fn x y) | x <- [False, True], y <- [False, True] ]

## 5. Listes et chaînes de caractères

### A

> dropHello str = drop 6 str

### B

> hasHello str = take 5 str == "Hello"

### C

> clean s = if hasHello s then dropHello s else s

### D

> import Data.Char
> upperHead (c:cs) = toUpper c : cs
> upperHead [] = []

## 6. Tuples

### A

> tuple n = (carré n, carré (n+1), carré (n+2))

### B

> findAge :: [(String, Int)] -> Int
> findAge ((n, a):ls) name = if n == name then a else findAge ls name
> findAge [] _ = undefined

If the name is not in the list, an exception is risen. A better solution would
be to return a `Maybe Int`, where the empty value (`Nothing` can be returned
instead).

> findAge' :: [(String, Int)] -> Maybe Int
> findAge' ((n, a):ls) name = if n == name then Just a else findAge ls name
> findAge' [] _ = Nothing

## 7. Fonctions prédéfinies

> length xs = if null xs then 0 else 1 + length (tail xs)
> head xs = xs !! 0
> xs !! n = if n == 0 then head xs else (tail xs) !! (n-1)
> last xs = xs !! (length xs - 1)
> tail xs = drop 1 xs
> init xs = take (length xs - 2) xs
> take' n xs = if n == 0 then [] else (head xs) : (take' (n-1) (tail n-1))
