<!-- (C) Roosembert Palacios, 2020
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
-->
Lab 2
===

This file is markdown-flavored literate haskell.
Please compile with options `-optL -q`.

Boilerplate
====

Use of template haskell enables quickcheck to find all test cases.
```haskell
> {-# LANGUAGE TemplateHaskell #-}
```

Type-applying clears `-Wtype-defaults` warnings.
```haskell
> {-# LANGUAGE TypeApplications #-}
```

Various imports
```haskell
> import Control.Monad
> import System.Exit
> import Test.QuickCheck
```

8) Sous-liste
====

```haskell
> slice :: String -> Integer -> Integer -> String
> slice str from to = [x | (i, x) <- zip [1..] str, i > from, i <= to]
> slice' :: String -> Integer -> Integer -> String
> slice' [] _ _ = []
> slice' _ _ 0 = []
> slice' (x:xs) 0 to = x : slice' xs 0 to
> slice' (_:xs) n to = slice' xs (n-1) to
```

Test:

**Note**: This seems to be wrong in the lab handout.

```haskell
> prop_slice :: Property
> prop_slice = slice "ello world!" 5 10 === "world"
> prop_slice' :: Property
> prop_slice' = slice' "ello world!" 5 10 === "world"
```

9) Insertion
====

```haskell
> insert :: a -> Integer -> [a] -> [a]
> insert e 1 xs = e:xs
> insert e _ [] = [e]
> insert e n (x:xs) = x : insert e (n-1) xs
```

Test:

```haskell
> prop_insert :: Property
> prop_insert = insert 'd' 4 "abcef" === "abcdef"
```

10) Filtre de multiples
====

```haskell
> sieve :: Integer -> [a] -> [a]
> sieve n xs = [x | (i, x) <- zip [0..] xs, i `mod` n == 0 ]
```

Test:

```haskell
> prop_sieve :: Property
> prop_sieve = sieve @Int 2 [1..9] === [1,3,5,7,9]
```

11) Nombres premiers
====

```haskell
> prime :: Integer -> Bool
> prime n = null [x | x <- [2..ul], n `mod` x == 0]
>   where ul = round @Double . sqrt $ fromInteger n
```

Test:

```haskell
> prop_prime :: Property
> prop_prime = prime 12 === False
```

12) Conjecture de Goldback
====

```haskell
> parts :: Integer -> [(Integer, Integer)]
> parts n = [(a, b) | (a, b) <- [(a, b) | a <- primes, b <- primes], a + b == n]
>   where primes = filter prime [2..n]
```

Test:

```haskell
> prop_parts :: Property
> prop_parts = parts 14 === [(3,11), (7,7), (11,3)]
```

13) Types
====

```haskell
3 * pi :: Double
```

```
(1.5,"3") :: !
```
Type error: No instance of Fractional for Char.
In particular `Double` != `Char`.

```
head "Hello " ++ "World !" :: !
```
Type error: `head "Hello "` is of type `Char`. `Char` != `[Char]`.

14) Motifs
====

```haskell
> add :: (Num a, Num b) => (a, b) -> (a, b) -> (a, b)
> add (a1, b1) (a2, b2) = (a1 + a2, b1 + b2)

> prod :: Num a => (a, a, a) -> (a, a, a) -> a
> prod (a1, b1, c1) (a2, b2, c2) = a1 * a2 + b1 * b2 + c1 * c2

> add' :: Num a => [(a, a)] -> [(a, a)] -> [(a, a)]
> add' [] _ = []
> add' _ [] = []
> add' (x:xs) (y:ys) = add x y : add' xs ys

> prod' :: Num a => [(a, a, a)] -> [(a, a, a)] -> [a]
> prod' [] _ = []
> prod' _ [] = []
> prod' (x:xs) (y:ys) = prod x y : prod' xs ys

> head' :: [a] -> a
> head' [] = undefined
> head' (a:_) = a

> tail' :: [a] -> [a]
> tail' [] = undefined
> tail' (_:as) = as

> elem5 :: [a] -> a
> elem5 (_:_:_:_:e:_) = e
> elem5 _ = undefined
```

Tests:

```haskell
> prop_add :: Int -> Int -> Int -> Int -> Property
> prop_add a b c d = add (a, b) (c, d) === (a + c, b + d)

> prop_prod :: Int -> Int -> Int -> Int -> Int -> Int -> Property
> prop_prod a b c d e f = prod (a, b, c) (d, e, f) === a*d + b*e + c*f

> prop_add' :: [(Int, Int)] -> [(Int, Int)] -> Property
> prop_add' as bs = add' as bs === [add a b | (a, b) <- zip as bs]

> prop_prod' :: [(Int, Int, Int)] -> [(Int, Int, Int)] -> Property
> prop_prod' as bs = prod' as bs === [prod a b | (a, b) <- zip as bs]

> prop_head' :: [Int] -> Property
> prop_head' as = not (null as) ==> head' as === head as

> prop_tail' :: [Int] -> Property
> prop_tail' as = not (null as) ==> tail' as === tail as

> prop_elem5 :: [Int] -> Property
> prop_elem5 as = not (null $ drop 4 as) ==> elem5 as === head (drop 4 as)
```

15) Gardes
====

```haskell
> pgcd :: Integer -> Integer -> Integer
> pgcd a b | a == b = a
> pgcd a b | a > b = pgcd b (a - b)
> pgcd a b = pgcd a (b - a)
```

Tests:

```haskell
> prop_pgcd :: Integer -> Integer -> Property
> prop_pgcd a b = a > 0 && b > 0 ==> pgcd a b === maximum commonDivisors
>   where commonDivisors = [ x | x <- [1..a], x `divides` a && x `divides` b ]
>         divides x num = num `mod` x == 0
```

16) Where
====


```haskell
> -- | A pentagon is a poligon composed of 5 isosceles triangles of 72°
> -- of the specified height.
> data Pentagon = Pentagon { height :: Double } deriving (Eq, Show)

> instance Arbitrary Pentagon where
>   arbitrary = Pentagon <$> arbitrary

> area :: Pentagon -> Double
> area (Pentagon tHeight) = 5 * (tBase * tHeight / 2)
>   where tBase = 2 * tHeight * tan (pi/5)
```

Test:

```haskell
> prop_area :: Pentagon -> Property
> prop_area p@(Pentagon h) = (area p) ~== (1/2 * perimeter * h)
>   where perimeter = 5 * 2 * h * tan (pi/5)
>         a ~== b = (truncate @_ @Int $ a * 1e9) === (truncate $ b * 1e9)
```

Instrumentation
===

Required so quickcheck can find the tests.

```haskell
> return []
```

Have the main function run the tests and return an adequate status code.

```haskell
> main :: IO ()
> main = $quickCheckAll >>= flip unless exitFailure
```

<!-- vim:ft=markdown
-->
