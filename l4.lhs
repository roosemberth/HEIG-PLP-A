<!-- This document may be rendered using the following commands:
pandoc l4.lhs -f markdown -s -o l4.pdf
pandoc l4.lhs -f markdown -s -o l4.html
-->
---
title: PLP-A 2020 Lab 3
author:
  - Roosembert Palacios
  - Mai Hoang Anh
---
<!-- (C) Roosembert Palacios, 2020

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
-->

This file is markdown-flavored literate haskell.
Please compile with options `-optL -q`. (e.g. `ghc -optL -q -Wall l4.lhs`.)

Boilerplate
----

Use of template haskell enables quickcheck to find all test cases.
```haskell
> {-# LANGUAGE TemplateHaskell #-}
```

Type-applying clears `-Wtype-defaults` warnings.
```haskell
> {-# LANGUAGE TypeApplications #-}
```

Various imports
```haskell
> import Control.Monad
> import Data.Char (toLower)
> import System.Exit

> -- QuickCheck >= 2
> import Test.QuickCheck
```

26) Projections
----

```haskell
> check :: (a -> b -> c) -> [(a, b)] -> [(a, b, c)]
> check f = map (uncurry joinResult)
>   where joinResult a b = (a, b, f a b)
```

```haskell
> fmap' :: [(a -> b)] -> a -> [b]
> fmap' fs e = map ($ e) fs
```

```haskell
> listes :: [a] -> [[a]]
> listes = map (flip (:) [])
```

Tests:

```haskell
> prop_check :: [Int] -> [Int] -> Property
> prop_check as bs = check (+) (zip as bs) === [(a, b, a + b) | (a, b) <- zip as bs]
```

```haskell
> prop_fmap' :: Int -> Property
> prop_fmap' v = fmap' [succ, pred, (+1)] v === [succ v, pred v, v + 1]
```

```haskell
> prop_listes :: (Eq a, Show a) => [a] -> Property
> prop_listes ls = fmap head (listes ls) === ls
```

27) Projections
----

```haskell
> carres :: (Integral a, Functor t) => t a -> t a
> carres = fmap (join (*))
```

```haskell
> positiveAttitude :: (Num a, Functor t) => t a -> t a
> positiveAttitude = fmap abs
```

```haskell
> strFst5 :: [String] -> [String]
> strFst5 = fmap (take 5)
```

```haskell
> minuscules :: String -> String
> minuscules [] = []
> minuscules (c:cs) = c : fmap toLower cs
```

Tests:

```haskell
> prop_carres :: (Show a, Eq a, Integral a) => [a] -> Property
> prop_carres l = carres l === [ x*x | x <- l ]
```

```haskell
> prop_positiveAttitude :: (Eq a, Show a, Ord a, Num a) => [a] -> Property
> prop_positiveAttitude l = property $ all (>= 0) (positiveAttitude l)
```

```haskell
> prop_strFst5 :: [String] -> Property
> prop_strFst5 strs = all ((4 <) . length) strs ==>
>   property $ all ((5 ==) . length) (strFst5 strs)
```

```haskell
> prop_minuscules :: String -> Property
> prop_minuscules str = property $ case str of
>   [] -> minuscules str == []
>   _ -> (head r) == (head str) && all (not . (`elem` ['A'..'Z'])) (tail r)
>     where r = minuscules str
```

28) Filtrage
----

```haskell
> isPair :: Integer -> Bool
> isPair e = e `mod` 2 == 0

> filterPair :: [Integer] -> [Integer]
> filterPair = filter isPair

> filterInpair :: [Integer] -> [Integer]
> filterInpair = filter (not . isPair)

> cherche :: (a -> Bool) -> [a] -> a
> cherche predicate = head . filter predicate
```

Tests:

```haskell
> prop_filterPair :: [Integer] -> Property
> prop_filterPair l = property $ all (isPair) (filterPair l)

> prop_filterInpair :: [Integer] -> Property
> prop_filterInpair l = property $ all (not . isPair) (filterInpair l)

> prop_cherche :: NonEmptyList Integer -> Property
> prop_cherche (NonEmpty ls) = any predicate ls ==> property $
>   predicate e && (all (not . predicate) $ takeWhile (/= e) ls)
>   where e = cherche predicate ls
>         predicate = (> 5)
```

29) Pliages
----

```haskell
> existe :: [t] -> (t -> Bool) -> Bool
> existe ls = foldr (||) False . flip fmap ls

> tous :: [t] -> (t -> Bool) -> Bool
> tous ls = foldr (&&) True . flip fmap ls
```

Tests:

```haskell
> prop_existe :: [Integer] -> Property
> prop_existe ns = any fn ns === existe ns fn
>   where fn = (> 5)

> prop_tous :: [Integer] -> Property
> prop_tous ns = classify res "all elements" $
>   property $ case res of
>     True -> tous ns fn
>     False -> not $ tous ns fn
>   where res = all fn ns
>         fn = (> 5)
```

Instrumentation
----

Required so quickcheck can find the tests.

```haskell
> return []
```

Have the main function run the tests and return an adequate status code.

```haskell
> main :: IO ()
> main = $quickCheckAll >>= flip unless exitFailure
```

<!-- vim:ft=markdown
-->
