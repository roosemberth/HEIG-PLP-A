# l9

This project is the implementation of a Lexer, Parser, REPL and Interpreter of
a custom programming language. It's mostly white-space insensitive at the
expense of some syntax being more explicit

## Running

A VT-100-compatible repl may be run using the following command:

```
$ stack run
```

Stack will then build the project and you'll be dropped into a repl. It supports
most command-line editing features you'd expect. Including command history.

```
SPL > 1 + 2
3
SPL > # This is a comment :D
SPL > # Try some emacs shortcuts ;-)
SPL > def @fn($n){$n+1}
SPL > @fn(2)
3
SPL > let my $var = 4 in @fn($var)
5
```

You may exit the REPL using `C-d`.

> If you experience problems with the REPL, you may run a minimalistic repl
> using the command `stack run simple-repl`. Note that this repl has no support
> for any command-line editing features.

You may also pipe programs to l9 for evaluation. try running
`echo 5 + 10 | stack run` !

You may also specify programs to run as an argument. Try running the examples
using `stack run -- ./res/examples/e1.spl`.

## Language

The language is very simple, it only supports terms of type int.

### Function definitions

```
def @fun($v1) {
  # The last expression will be used as return value of the function.
  $v1 + ($v2 * 2)
}
```

### Variable declaration

```
my $var = 5
```

### ternary expressions

```
my $var = (if $foo > 0 then $var + 1 else $var - 1)
```

> Note the surrounding parenthesis are part of the syntax.

## Invoking

Start the repl: `stack run`

Interpret a program: `stack run -- ./res/examples/e1.spl`

You can also pipe a program: `cat ./res/examples/e1.spl | stack run`
