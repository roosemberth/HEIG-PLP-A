{-# LANGUAGE LambdaCase #-}
module SPL.Interpreter where

import Control.Applicative (liftA2)
import Control.Monad (foldM)
import Data.Function (on)
import SPL.Lexer (lexSPL)
import SPL.Parser hiding (main)

-- | Inhabitants types of the language
type Term = Int

-- | Append-only environment. No masking checks are done.
type Env = [Binding]

data Function = Function { name :: String, bindEnv :: [String], code :: SPL }
  deriving Show

lookupFn :: Env -> String -> Either String Function
lookupFn [] sym               = Left $ "Function " <> sym <> " not in scope."
lookupFn (VarDef _ _:env) sym = lookupFn env sym
lookupFn (FnDef name args code:env) sym
  | name == sym = Right $ Function name args code
  | otherwise   = lookupFn env sym

lookupVal :: Env -> String -> Either String Expr
lookupVal [] sym            = Left $ "Variable " <> sym <> " not in scope."
lookupVal (FnDef{}:env) sym = lookupVal env sym
lookupVal (VarDef name expr:env) sym
  | name == sym = Right expr
  | otherwise   = lookupVal env sym

boolToTerm :: Bool -> Term
boolToTerm True  = 1
boolToTerm False = 0

on' :: Applicative f => (a -> a -> c) -> (b -> f a) -> b -> b -> f c
on' = on . liftA2

evalOp :: Env -> Operation -> Either String Term
evalOp env (OpPlus  e1 e2) = on' (+) (evalExpr env) e1 e2
evalOp env (OpMinus e1 e2) = on' (-) (evalExpr env) e1 e2
evalOp env (OpDiv   e1 e2) = on' div (evalExpr env) e1 e2
evalOp env (OpMult  e1 e2) = on' (*) (evalExpr env) e1 e2
evalOp env (OpMod   e1 e2) = on' mod (evalExpr env) e1 e2
evalOp env (OpLT    e1 e2) = boolToTerm <$> on' (>) (evalExpr env) e1 e2
evalOp env (OpGT    e1 e2) = boolToTerm <$> on' (>) (evalExpr env) e1 e2
evalOp env (OpGET   e1 e2) = boolToTerm <$> on' (>=) (evalExpr env) e1 e2
evalOp env (OpLET   e1 e2) = boolToTerm <$> on' (<=) (evalExpr env) e1 e2
evalOp env (OpEq    e1 e2) = boolToTerm <$> on' (==) (evalExpr env) e1 e2
evalOp env (OpNEq   e1 e2) = boolToTerm <$> on' (/=) (evalExpr env) e1 e2
evalOp env (OpNeg   expr) = boolToTerm . not <$> evalCond env expr

evalCond :: Env -> Expr -> Either String Bool
evalCond env expr = (/= 0) <$> evalExpr env expr

isFunction :: Binding -> Bool
isFunction FnDef{} = True
isFunction _       = False

applyFn :: Env -> [Term] -> Function -> Either String Term
applyFn env callArgs (Function name bindEnv code)
  | length callArgs > length bindEnv =
    Left $ "Too many arguments when calling function " <> name <> "."
  | length callArgs < length bindEnv =
    Left $ "Missing arguments when calling function " <> name <> "."
  | otherwise = run (zipWith VarDef bindEnv (Lit <$> callArgs) <> fns) code >>= \case
      (_, []) -> Left $ "Function " <> name <> " failed to produce a result."
      (_, ls) -> Right (last ls)
  where
    -- This is a big ugly hack: code should be compiled after parsing and
    -- functions should be discharged and linked then. Functions in the current
    -- environment are not necessarily the ones the programer intended or had
    -- access to at the definition site, especially since the environment may
    -- be modified by scoped blocks (or even other modules).
    fns = filter isFunction env

evalExpr :: Env -> Expr -> Either String Term
evalExpr env (IfThenElse c t f)    =
  evalCond env c >>= \p -> evalExpr env $ if p then t else f
evalExpr env (FnApp fn args)       = do
  fn <- lookupFn env fn
  dischargedArgs <- mapM (evalExpr env) args
  applyFn env dischargedArgs fn
evalExpr env (OpApp op)            = evalOp env op
evalExpr env (ScopedEnv env' expr) = evalExpr (mappend env' env) expr
evalExpr env (Var v)               = lookupVal env v >>= evalExpr env
evalExpr _   (Lit v)               = pure v

-- | Resolves elements of a binding with the provided scope
dischargeBinding :: Env -> Binding -> Either String Binding
dischargeBinding env (VarDef sym expr) = VarDef sym . Lit <$> evalExpr env expr
-- | FIXME: This code should be discharged and linked. Not doing so forces the
-- interpreter to provide their environment which may differ from the one the
-- programer intended or had access to at definition time
dischargeBinding _ b = pure b

-- | Eval an SPL statement
evalStmt :: Env -> Statement -> Either String (Env, [Term])
evalStmt env (Binding b)     =
  (\b' -> (b':env, [])) <$> dischargeBinding env b
evalStmt env (Cond expr t f) =
  evalCond env expr >>= \p -> run env $ if p then t else f
evalStmt env (TopExpr expr)  = (\r -> (env, [r])) <$> evalExpr env expr

-- | Run an SPL Program
run :: Env -> SPL -> Either String (Env, [Term])
run initEnv = foldM eval' (initEnv, [])
  where eval' (env, accum) statement =
          fmap (mappend accum) <$> evalStmt env statement

lexAndParse :: String -> Either String SPL
lexAndParse program = lexSPL program >>= parseSPL
