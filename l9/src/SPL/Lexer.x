{
module SPL.Lexer where
}

%wrapper "monad"

$digit      = 0-9
$letter     = [A-Za-z]

@reservedid = if|then|else|my|def|let|in
@operators  = "+"|"-"|"/"|"*"|"%"|"<"|">"|">="|"<="|"=="|"!="|"!"
@reservedop = ","|"="|@operators|"`"|"("|")"|"{"|"}"
@genSymbol  = $letter ( $letter | $digit )*

@varsym = "$" @genSymbol
@fnsym  = "@" @genSymbol

:-

$white+     ;
"#".*       ; -- Don't lex comments
@reservedid { mkL LReserved }
@varsym     { mkL LVarSym }
@fnsym      { mkL LFnSym }
@reservedop { mkL LReserved }
$digit+     { mkL LLit }

{
data Lexeme = L AlexPosn LexemeClass String
  deriving (Eq, Show)

data LexemeClass
  = LEOF
  | LVarSym
  | LFnSym
  | LReserved
  | LLit
  deriving (Eq, Show)

mkL :: LexemeClass -> AlexInput -> Int -> Alex Lexeme
mkL c (p,_,_,str) len = return (L p c (take len str))

lexSPL src = runAlex src $ do
  let loop = alexMonadScan >>= cont
      cont tok@(L _ LEOF _) = return [] -- Don't include EOF
      cont tok              = (tok:) <$> loop
  loop

alexEOF = return (L undefined LEOF "")

showPosn (AlexPn _ line col) = show line ++ ':': show col

main = getContents >>= print . lexSPL
}
