{
module SPL.Parser
  ( Binding(..)
  , Expr(..)
  , Operation(..)
  , Statement(..)
  , SPL
  , parseSPL
  ) where

import SPL.Lexer hiding (main)
}

%name parseSPL
%tokentype { Lexeme }
%monad { Either String }
%error { parseError }

%token
  let     { L _ LReserved "let" }
  in      { L _ LReserved "in" }
  if      { L _ LReserved "if" }
  then    { L _ LReserved "then" }
  else    { L _ LReserved "else" }
  ','     { L _ LReserved "," }
  '='     { L _ LReserved "=" }
  '`'     { L _ LReserved "`" }
  '('     { L _ LReserved "(" }
  ')'     { L _ LReserved ")" }
  '{'     { L _ LReserved "{" }
  '}'     { L _ LReserved "}" }
  varsym  { L _ LVarSym $$ }
  fnsym   { L _ LFnSym $$ }
  literal { L _ LLit $$ }
  '+'     { L _ LReserved "+" }
  '-'     { L _ LReserved "-" }
  '/'     { L _ LReserved "/" }
  '*'     { L _ LReserved "*" }
  '%'     { L _ LReserved "%" }
  '<'     { L _ LReserved "<" }
  '>'     { L _ LReserved ">" }
  '>='    { L _ LReserved ">=" }
  '<='    { L _ LReserved "<=" }
  '=='    { L _ LReserved "==" }
  '!='    { L _ LReserved "!=" }
  '!'     { L _ LReserved "!" }
  my      { L _ LReserved "my" }
  def     { L _ LReserved "def" }

%%

SPL :            { [] }
    | VarDef SPL { (Binding $1):$2 }
    | FnDef SPL  { (Binding $1):$2 }
    | Cond SPL   { $1:$2 }
    | Expr SPL   { (TopExpr $1):$2 }

VarDef : my varsym '=' Expr   { VarDef $2 $4 }

FnDef     : def fnsym '(' FnDefArgs ')' '{' SPL '}' { FnDef $2 $4 $7 }
FnDefArgs :                      { [] }
          | varsym               { [$1] }
          | varsym ',' FnDefArgs { $1:$3 }

Cond : if ParExpr then '{' SPL '}' else '{' SPL '}' { Cond $2 $5 $9 }

Expr : ParExpr                { $1 }
     | Term                   { $1 }
     | ScopedEnv              { $1 }
     | IfThenElse             { $1 }
     | FnApp                  { $1 }
     | InfixApp               { $1 }

ParExpr : '(' Expr ')'        { $2 }

Term : varsym                 { Var $1 }
     | literal                { Lit (read $1) }

ScopedEnv   : let EnvBindings in Expr { ScopedEnv $2 $4 }
EnvBindings :                    { [] }
            | VarDef EnvBindings { $1:$2 }
            | FnDef EnvBindings  { $1:$2 }

IfThenElse : '(' if Expr then Expr else Expr ')' { IfThenElse $3 $5 $7 }

FnApp     : fnsym '(' FnAppArgs ')' { FnApp $1 $3 }
FnAppArgs :                         { [] }
          | Expr                    { [$1] }
          | Expr ',' FnAppArgs      { $1:$3 }

InfixApp : InfixArg InfixFn InfixArg { FnApp $2 [$1, $3] }
         | InfixArg InfixOp InfixArg { OpApp ($2 $1 $3) }
         | '!' varsym                { OpApp (OpNeg (Var $2)) }

InfixArg : Term    { $1 }
         | ParExpr { $1 }
         | FnApp   { $1 }

InfixFn  : '`' fnsym '`' { $2 }

InfixOp  : '+'     { OpPlus }
         | '-'     { OpMinus }
         | '/'     { OpDiv }
         | '*'     { OpMult }
         | '%'     { OpMod }
         | '<'     { OpLT }
         | '>'     { OpGT }
         | '>='    { OpGET }
         | '<='    { OpLET }
         | '=='    { OpEq }
         | '!='    { OpNEq }

{
parseError :: [Lexeme] -> Either String a
parseError [] = Left "Parse error: empty parse"
parseError (l:_) = Left $ "Parse error @" ++ show l

data Operation
  = OpPlus  Expr Expr
  | OpMinus Expr Expr
  | OpDiv   Expr Expr
  | OpMult  Expr Expr
  | OpMod   Expr Expr
  | OpLT    Expr Expr
  | OpGT    Expr Expr
  | OpGET   Expr Expr
  | OpLET   Expr Expr
  | OpEq    Expr Expr
  | OpNEq   Expr Expr
  | OpNeg   Expr
  deriving Show

data Expr
  = IfThenElse Expr Expr Expr
  | FnApp String [Expr]
  | OpApp Operation
  | ScopedEnv [Binding] Expr
  | Var String
  | Lit Int
  deriving Show

data Binding
  = VarDef String Expr
  | FnDef String [String] SPL
  deriving Show

data Statement
  = Binding Binding
  | Cond Expr SPL SPL
  | TopExpr Expr
  deriving Show

type SPL = [Statement]

main = getContents >>= print . (\p -> lexSPL p >>= parseSPL)
}
