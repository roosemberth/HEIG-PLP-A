-- (C) Roosembert Palacios, 2019
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- This file is an extract of an exercise on chapter 10 of the book
-- “Programming in Haskell” By Graham Hutton. You can find the rest of the
-- exercises on https://gitlab.com/roosemberth/programming-in-haskell---exercises


module ReadLine (TermAction(..), readLine) where

import Data.Char (ord)
import Data.Maybe (fromMaybe)
import System.IO (hFlush, BufferMode(..), hSetBuffering, stdout, hSetEcho, stdin)

getCh :: IO Char
getCh = do hSetEcho stdin False
           x <- getChar
           hSetEcho stdin True
           return x


backwardsCode :: Int -> String
backwardsCode 0 = ""
backwardsCode n = "\ESC[" ++ show n ++ "D"

killline :: String
killline = "\ESC[K"

clearScreen :: String
clearScreen = "\ESC[2J\ESC[H"

clearLine :: String
clearLine = "\ESC[2K\ESC[F\ESC[1B"

safetail n = if null n then n else tail n
safeinit n = if null n then n else init n
safehead n = [ head n | not (null n)]
safelast n = [last n | not (null n)]

deletenextword [] = []
deletenextword nb@(c:cs) | c == ' '  = deletenextword cs
                         | otherwise = safetail $ dropWhile (/= ' ') nb
jumptonextword []                    = ([],[])
jumptonextword nb@(c:cs) | c == ' '  = case jumptonextword cs of (a, b) -> (c:a, b)
                         | otherwise = span (/= ' ') nb

data Ops = BS | DEL | DROPHOLD | ENTER | HOLD | LEFT | RIGHT | TYPE -- Base nav
         | C_A | C_E | C_F | C_B | C_K | C_U | C_W | C_D | C_L | C_P | C_N
         | A_F | A_B | A_D
  deriving Eq

class Console a where
  ingest     :: Char -> [Char] -> a
  evolveHold :: a -> Char -> [Char] -> [Char] -- Hold buffer
  evolveBufs :: a -> Char -> ([Char],[Char]) -> ([Char],[Char]) -- Prev/Next buffer

instance Console Ops where
  ingest c [] | c == '\b'   = BS
              | c == '\DEL' = BS
              | c == '\n'   = ENTER
              | c == '\ESC' = HOLD
              | c == '\SOH' = C_A
              | c == '\ENQ' = C_E
              | c == '\ACK' = C_F
              | c == '\STX' = C_B
              | c == '\v'   = C_K
              | c == '\NAK' = C_U
              | c == '\ETB' = C_W
              | c == '\EOT' = C_D
              | c == '\FF'  = C_L
              | c == '\DLE' = C_P
              | c == '\SO'  = C_N
              | otherwise   = TYPE
  ingest c hold@(_:_) | c == '\n' = ENTER
                      | otherwise = holding (hold++[c])
      where holding cs | cs == "\ESC[3~" = DEL
                       | cs == "\ESC[C"  = RIGHT
                       | cs == "\ESC[D"  = LEFT
                       | cs == "\ESCf"   = A_F
                       | cs == "\ESCb"   = A_B
                       | cs == "\ESCd"   = A_D
                       | length cs > 4   = DROPHOLD
                       | otherwise       = HOLD

  evolveHold HOLD c hb = hb ++ [c]
  evolveHold _ _ _     = []
  evolveBufs BS       c (pb,nb) = (safeinit pb,nb)
  evolveBufs DEL      c (pb,nb) = (pb,safetail nb)
  evolveBufs DROPHOLD c (pb,nb) = (pb,nb)
  evolveBufs ENTER    c (pb,nb) = (pb ++ nb,[])
  evolveBufs HOLD     c (pb,nb) = (pb,nb)
  evolveBufs LEFT     c (pb,nb) = (safeinit pb,safelast pb ++ nb)
  evolveBufs RIGHT    c (pb,nb) = (pb ++ safehead nb,safetail nb)
  evolveBufs TYPE     c (pb,nb) = (pb ++ [c],nb)
  evolveBufs C_A      _ (pb,nb) = ([],pb ++ nb)
  evolveBufs C_E      _ (pb,nb) = (pb ++ nb,[])
  evolveBufs C_F      _ (pb,nb) = (pb ++ safehead nb,safetail nb)
  evolveBufs C_B      _ (pb,nb) = (safeinit pb,safelast pb ++ nb)
  evolveBufs A_F      _ (pb,nb) = case jumptonextword nb of (a,b) -> (pb++a,b)
  evolveBufs A_B      _ (pb,nb) =
    case jumptonextword (reverse pb) of (a,b) -> (reverse b, reverse a ++ nb)
  evolveBufs A_D      _ (pb,nb) = (pb,deletenextword nb)
  evolveBufs C_K      _ (pb,nb) = (pb,[])
  evolveBufs C_U      _ (pb,nb) = ([],nb)
  evolveBufs C_W      c (pb,nb) =
    case jumptonextword (reverse pb) of
      (a,b) -> (reverse b, deletenextword $ reverse a ++ nb)
  evolveBufs _        _ (pb,nb) = (pb,nb)

data TermAction
  = Exit
  -- | The terminal was cleared, str was in the buffer.
  -- Usually you just want to redraw.
  | ClearedTerm String
  -- | User requested the next element in the history
  -- Contains partial user input
  -- The current line was cleared
  -- Usually you want to redraw with the next line in the history
  | HistoryNext String
  -- | User requested the previous element in the history
  -- Contains partial user input
  -- The current line was cleared
  -- Usually you want to redraw with the previous line in the history
  | HistoryPrev String

-- | Reads a line from the user. Allowing them to edit the current line.
readLine :: String -> IO (Either TermAction String)
readLine inputBuffer = do
  hSetBuffering stdout NoBuffering
  hSetBuffering stdin NoBuffering
  putStr inputBuffer
  hFlush stdout
  getCh >>= readline' [] inputBuffer []
  where displayline :: Int -> [Char] -> Int -> IO ()
        displayline po t co = do
          putStr $ backwardsCode po ++ killline ++ t ++ backwardsCode co
        readline' :: [Char] -> [Char] -> [Char] -> Char -> IO (Either TermAction String)
        readline' h p n c = do
          let op = ingest c h
              h' = evolveHold op c h
              (p',n') = evolveBufs op c (p,n)
          displayline (length p) (p'++n') (length n')
          case () of
            () | op == ENTER -> return (Right p')
            () | op == C_D   -> return (Left Exit)
            () | op == C_L   -> do
              putStr clearScreen
              return (Left (ClearedTerm p'))
            () | op == C_P   -> do
              putStr clearLine
              return (Left (HistoryPrev p'))
            () | op == C_N   -> do
              putStr clearLine
              return (Left (HistoryNext p'))
            _                -> getCh >>= readline' h' p' n'
