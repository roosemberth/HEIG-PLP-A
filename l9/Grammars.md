# Question N.46 (Grammaires)

G1: [ "0" ] "1" | { "1" } | { G1 }

G2: "1", G2, "1" | "0", G2, "0" | { "0" } | { "1" }

G3 is not exhaustive for L3, since L3 is not regular.
G3: "01", { G3 } | "10", { G3 } | "1", { G3 }, "0" | "0", { G3 }, "1" | [ "01" ]

G4 is not exhaustive for L3, since L4 is not regular.
G4: { "1" }, { G3 } | { "0" }, { G3 } | { G3 }, { "1" } | { G4 }, { "0" } | { "1" } | { "0" }

# Question N.47 (Expressions)

Note: G47 requires association to prevent ambiguities

G47: G47_bool_expr | terme
G47_bool_expr: G47_bool_term | G47_disjunction, | G47_conjunction
G47_bool_term: "True" | "False"
G47_disjunction: G47_bool_term_or_guarded_expr, "||", G47_bool_term_or_guarded_expr
G47_conjunction: G47_bool_term_or_guarded_expr, "&&", G47_bool_term_or_guarded_expr
G47_bool_term_or_guarded_expr: G47_bool_term | "(", G47_bool_expr, ")"

# Question N.48 (Simple Programming Language)

- un opérateur original, de votre cru,

```ebnf
SPL = { SPL } { SPL_line_end }
     | SPL_vardefinition SPL_line_end
     | SPL_fndefinition SPL_line_end
     | SPL_cond
     | SPL_expr SPL_line_end
     | "MESSAGE \"" { notnewline } "\""
     ;

SPL_line_end           = { " " } { SPL_line_end_comment };
SPL_line_end_comment   = "#" notnewline "\n";
SPL_spolend = { " " } | SPL_line_end; (* Spaces or line end *)

SPL_vardefinition = "my " SPL_varsymbol " = " SPL_expr;
SPL_fndefinition  = "def " SPL_fnsymbol { " " }
                    "(" SPL_arg_list ")" { SPL_spolend }
                    "{" { SPL } "}";

SPL_arg_list = { SPL_varsymbol ", " } [ SPL_varsymbol ];

SPL_varsymbol      = "$" SPL_generic_symbol;
SPL_fnsymbol       = "@" SPL_generic_symbol;
SPL_generic_symbol = letter { letter | digit | "'" | "_" };

SPL_cond = "if" { SPL_spolend } "(" SPL_expr ")" { SPL_spolend }
           "then" { SPL_spolend } "{" SPL "}" { SPL_spolend }
           { "else" { SPL_spolend } "{" SPL "}" { SPL_spolend } };

SPL_expr  = SPL_parexpr
          | SPL_if_then_else
          | SPL_fnapp
          | SPL_infixapp
          | SPL_env_block
          | SPL_term
          ;

SPL_parexpr = "(" { " " } SPL_expr { " " } ")";
SPL_term = SPL_varsymbol
         | SPL_literal;

SPL_if_then_else = "(if" SPL_expr "then" SPL_expr "else" SPL_expr ")";

SPL_fnapp = SPL_fnsymbol "(" { SPL_expr "," } [ SPL_expr] ")";

SPL_infixapp = SPL_infixarg { " " } SPL_infixsym { " " } SPL_infixarg
             | "!" { " " } SPL_infixarg;
SPL_infixarg = SPL_parexpr | SPL_term;
SPL_infixsym = ? [+-/*%<>] ? | ">=" | "<=" | "==" | "!=" | SPL_infiedfn;
SPL_infiedfn = "`" SPL_fnsymbol "`";

SPL_env_block = "let " { SPL_env_block_binding } { SPL_spolend { " " } }
                "in " { SPL_line_end } SPL_expr SPL_line_end;
SPL_env_block_binding = { SPL_spolend }
                        (SPL_vardefinition | SPL_fndefinition)
                        { SPL_line_end SPL_env_block_binding };

SPL_literal = { digit } digit;

notnewline = ? [^\n]* ?;
letter = ? [A-Za-z] ?;
digit = ? [0-9] ?;
```

```SPL
def @factoriel($n) { (if $n == 1 then 1 else @factoriel($n-1)) }
```
