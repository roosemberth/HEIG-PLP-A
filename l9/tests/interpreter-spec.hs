import Data.Either
import Test.Hspec
import SPL.Interpreter

spl :: String -> Either String [Term]
spl code = snd <$> run [] parsed
  where Right parsed = lexAndParse code

testVariables :: Spec
testVariables = describe "Variables" $ do
  it "should hold values" $ do
    spl "my $var = 1\n$var"
    `shouldBe`
    Right [1]
  it "should fail to bind variables out of scope" $ do
    spl "my $var = $foo"
    `shouldSatisfy`
    isLeft
  it "should resolve functions upon binding" $ do
    spl "def @foo() { 1 } my $bar = @foo() $bar"
    `shouldBe`
    Right [1]

testFunctions :: Spec
testFunctions = describe "Functions" $ do
  it "should allow recustion" $ do
    spl "def @sum($n) { (if $n == 0 then 0 else $n + @sum($n-1)) } @sum(10)"
    `shouldBe`
    Right [55]
  it "should alow cross recursion" $ do
    spl "def @f1($a) { (if $a == 0 then 0 else $a + @f2($a - 1)) } \
        \def @f2($a) { @f1($a) } \
        \@f1(10)"
    `shouldBe`
    Right [55]
  it "should return the last top-expr" $ do
    spl "def @f1() { 1 2 3 } @f1()"
    `shouldBe`
    Right [3]

testScopes :: Spec
testScopes = describe "Scopes" $ do
  it "should be discharged at bind-time" $ do
    spl "my $foo = 1 let my $bar = $foo in $bar"
    `shouldBe`
    Right [1]
  it "should not affect surrounding scope" $ do
    spl "my $foo = 1 let my $foo = 2 in 0 $foo"
    `shouldBe`
    Right[0, 1]
  it "should mask outer scope" $ do
    spl "my $foo = 1 let my $foo = 2 in $foo"
    `shouldBe`
    Right[2]

testInterpreter :: Spec
testInterpreter = describe "Interpreter" $ do
  it "should return results in execution order" $ do
    spl "1 2 3"
    `shouldBe`
    Right [1, 2, 3]

testExamples :: Spec
testExamples = describe "Example programs" $ do
  it "factorial" $ do
    spl "def @factorial($n) {\
        \  let my $r = (if $n <= 1 then 1 else @factorial($n-1) * $n)\
        \  in $r\
        \}\
        \@factorial(5)"
    `shouldBe`
    Right [120]

main :: IO ()
main = hspec $ do
  testVariables
  testFunctions
  testScopes
  testInterpreter
  testExamples
