---
title: PLP-A 2020 laboratoires 9 à 11
author:
  - Roosembert Palacios
  - Mai Hoang Anh
geometry:
  - a4paper
  - bindingoffset=0mm
  - inner=30mm
  - outer=30mm
  - top=25mm
  - bottom=10mm
---

# SPL

In the scope of the PLP course we designed and implemented a custom programming
language (SPL: Simple programming language). Our design was heavely influenced both
syntactically and semantically to perl and scala. Lexing and parsing was done
using Alex and Happy (as required in the project handout), the interpreter is
completely home-made and implemented in haskell. A relatively ergonomic REPL
was also added at the final stages of the project, featuring command-line
editing shortcuts such as C-w, C-a, M-d, C-k, ... and REPL history.

## Lexemes

Our lexer is extremely simple and performs little to none syntax analysis:

```
$white+     # Ignore whitespaces
"#".*       # Ignore end-line comments
@reservedid # Reserved keywords
@varsym     # Variable symbols: alphanum words starting with '$' and a letter.
@fnsym      # Function symbols: alphanum words starting with '@' and a letter.
@reservedop # Reserved operators and symbols
$digit+     # Literal values: numbers
```
Reserved keywords: `if` `then` `else` `my` `def` `let` `in`.

Reserved operators: ` ` `=` `+` `-` `/` `*` `%` `<` `>` `>=` `<=` `==` `!=` `!` \texttt{`} `(` `)` `{` `}`

We purposefully generate a lex error when a symbol is referenced without its
prefix (`@` or `$`). We intended to extend the scope of the lexer (and in
particular its errors), but were discouraged since it falls beyond the scope of
the project.

## Example code

Factorial function (extract from `./res/examples/e1.spl`):

```spl
def @factorial($n) {
  if ($n <= 1) then {
    1
  } else {
    $n * @factorial($n - 1)
  }
}
```

Mutual recursion (extract from `./res/examples/e5.spl`):

```spl
def @isEven($n) { (if $n == 0 then 1 else @isOdd($n-1)) }
def @isOdd($p) { (if $p == 0 then 0 else @isEven($p-1)) }
```

(We encourage the reader to see file `./res/examples/e6.spl`.)

\pagebreak

## Intermediate results: Lexemes & ASTs of examples

### Factorial

```
$ stack run -- ./res/examples/e1.spl
```

Lexemes: (See Annex)

AST:

```
[ Binding (
    FnDef "@factorial" ["$n"]
    [Cond (OpApp (OpLET (Var "$n") (Lit 1)))
      [TopExpr (Lit 1)]
      [TopExpr (OpApp (
          OpMult (Var "$n")
          (FnApp "@factorial"
            [OpApp (OpMinus (Var "$n") (Lit 1))]
          )
      ))]
    ])
, TopExpr (FnApp "@factorial" [Lit 0])
, TopExpr (FnApp "@factorial" [Lit 1])
, TopExpr (FnApp "@factorial" [Lit 2])
, TopExpr (FnApp "@factorial" [Lit 3])
, TopExpr (FnApp "@factorial" [Lit 4])
, TopExpr (FnApp "@factorial" [Lit 5])
, TopExpr (FnApp "@factorial" [Lit 6])
, TopExpr (FnApp "@factorial" [Lit 7])
, TopExpr (FnApp "@factorial" [Lit 8])
, TopExpr (FnApp "@factorial" [Lit 9])
, TopExpr (FnApp "@factorial" [Lit 10])
]
```

Results:

```
1
1
2
6
24
120
720
5040
40320
362880
3628800
```

\pagebreak

### Mutual recursion

```
$ stack run -- ./res/examples/e5.spl
```

Lexems: (See Annex)

AST:

```
[ Binding (
    FnDef "@isEven" ["$n"]
    [TopExpr (
      IfThenElse
        (OpApp (OpEq (Var "$n") (Lit 0)))
        (Lit 1)
        (FnApp "@isOdd" [OpApp (OpMinus (Var "$n") (Lit 1))])
    )]
  )
, Binding (
    FnDef "@isOdd" ["$n"]
    [TopExpr (
      IfThenElse
        (OpApp (OpEq (Var "$n") (Lit 0)))
        (Lit 0)
        (FnApp "@isEven" [OpApp (OpMinus (Var "$n") (Lit 1))])
    )]
  )
, TopExpr (FnApp "@isEven" [Lit 1])
, TopExpr (FnApp "@isEven" [Lit 2])
, TopExpr (FnApp "@isEven" [Lit 3])
, TopExpr (FnApp "@isEven" [Lit 5])
, TopExpr (FnApp "@isEven" [Lit 8])
, TopExpr (FnApp "@isEven" [Lit 13])
, TopExpr (FnApp "@isEven" [Lit 4181])
, TopExpr (FnApp "@isEven" [Lit 6765])
, TopExpr (FnApp "@isEven" [Lit 10946])
, TopExpr (FnApp "@isEven" [Lit 17711])
, TopExpr (FnApp "@isEven" [Lit 28657])
, TopExpr (FnApp "@isOdd" [Lit 46368])
]
```

Results:

```
0
1
0
0
1
0
0
0
1
0
0
0
```

\pagebreak

# Conclusion

During the development of the language, there were many areas left unattended
because of the scope of the project. In particular, the following features
are considered "low-hanging fruit" because of the structure of the inner
representation and the grammar design:

- Support for custom and algebraic types
- lambda functions
- Static lexical scope analysis
- static type-checking
- tail-recursion folding

These features were breifly explored but their results excluded from this
project because the modifications and haskell features used for their
implementation are out of the scope of this course.

\pagebreak

# Annex: ./res/examples/e1.spl

```spl
# This example uses branching to define the factorial function.
def @factorial($n) {
  if ($n <= 1) then {
    1
  } else {
    $n * @factorial($n - 1)
  }
}

@factorial(0)
@factorial(1)
@factorial(2)
@factorial(3)
@factorial(4)
@factorial(5)
@factorial(6)
@factorial(7)
@factorial(8)
@factorial(9)
@factorial(10)
```

# Annex: ./res/examples/e5.spl

```spl
# The following functions tests whether a
# number is odd or even using mutual recursion
def @isEven($n) { (if $n == 0 then 1 else @isOdd($n-1)) }
def @isOdd($n) { (if $n == 0 then 0 else @isEven($n-1)) }

@isEven(1)
@isEven(2)
@isEven(3)
@isEven(5)
@isEven(8)
@isEven(13)
@isEven(4181)
@isEven(6765)
@isEven(10946)
@isEven(17711)
@isEven(28657)
@isOdd(46368)
```

\pagebreak

# Annex: Question N.48 (EBNF SPL)

```ebnf
SPL = { SPL } { SPL_line_end }
     | SPL_vardefinition SPL_line_end
     | SPL_fndefinition SPL_line_end
     | SPL_cond
     | SPL_expr SPL_line_end
     | "MESSAGE \"" { notnewline } "\""
     ;

SPL_line_end           = { " " } { SPL_line_end_comment | "\n" };
SPL_line_end_comment   = "#" notnewline "\n";
SPL_spolend            = { " " } | SPL_line_end; (* Spaces or line end *)

SPL_vardefinition = "my " SPL_varsymbol " = " SPL_expr;
SPL_fndefinition  = "def " SPL_fnsymbol { " " }
                    "(" SPL_arg_list ")" { SPL_spolend }
                    "{" { SPL } "}";

SPL_arg_list = { SPL_varsymbol ", " } [ SPL_varsymbol ];

SPL_varsymbol      = "$" SPL_generic_symbol;
SPL_fnsymbol       = "@" SPL_generic_symbol;
SPL_generic_symbol = letter { letter | digit | "'" | "_" };

SPL_cond = "if" { SPL_spolend } "(" SPL_expr ")" { SPL_spolend }
           "then" { SPL_spolend } "{" SPL "}" { SPL_spolend }
           { "else" { SPL_spolend } "{" SPL "}" { SPL_spolend } };

SPL_expr  = SPL_parexpr
          | SPL_if_then_else
          | SPL_fnapp
          | SPL_infixapp
          | SPL_env_block
          | SPL_term
          ;

SPL_parexpr = "(" { " " } SPL_expr { " " } ")";
SPL_term = SPL_varsymbol
         | SPL_literal;

SPL_if_then_else = "(if" SPL_expr "then" SPL_expr "else" SPL_expr ")";

SPL_fnapp = SPL_fnsymbol "(" { SPL_expr "," } [ SPL_expr] ")";

SPL_infixapp = SPL_infixarg { " " } SPL_infixsym { " " } SPL_infixarg
             | "!" { " " } SPL_infixarg;
SPL_infixarg = SPL_parexpr | SPL_term;
SPL_infixsym = ? [+-/*%<>] ? | ">=" | "<=" | "==" | "!=" | SPL_infiedfn;
SPL_infiedfn = "`" SPL_fnsymbol "`";

SPL_env_block = "let " { SPL_env_block_binding } { SPL_spolend { " " } }
                "in " { SPL_line_end } SPL_expr SPL_line_end;
SPL_env_block_binding = { SPL_spolend }
                        (SPL_vardefinition | SPL_fndefinition)
                        { SPL_line_end SPL_env_block_binding };

SPL_literal = { digit } digit;

notnewline = ? [^\n]* ?;
letter = ? [A-Za-z] ?;
digit = ? [0-9] ?;
```

# Annex: Factorial function program Lexemes

```
[ L (AlexPn 64 2 1) LReserved "def"
, L (AlexPn 68 2 5) LFnSym "@factorial"
, L (AlexPn 78 2 15) LReserved "("
, L (AlexPn 79 2 16) LVarSym "$n"
, L (AlexPn 81 2 18) LReserved ")"
, L (AlexPn 83 2 20) LReserved "{"
, L (AlexPn 87 3 3) LReserved "if"
, L (AlexPn 90 3 6) LReserved "("
, L (AlexPn 91 3 7) LVarSym "$n"
, L (AlexPn 94 3 10) LReserved "<="
, L (AlexPn 97 3 13) LLit "1"
, L (AlexPn 98 3 14) LReserved ")"
, L (AlexPn 100 3 16) LReserved "then"
, L (AlexPn 105 3 21) LReserved "{"
, L (AlexPn 111 4 5) LLit "1"
, L (AlexPn 115 5 3) LReserved "}"
, L (AlexPn 117 5 5) LReserved "else"
, L (AlexPn 122 5 10) LReserved "{"
, L (AlexPn 128 6 5) LVarSym "$n"
, L (AlexPn 131 6 8) LReserved "*"
, L (AlexPn 133 6 10) LFnSym "@factorial"
, L (AlexPn 143 6 20) LReserved "("
, L (AlexPn 144 6 21) LVarSym "$n"
, L (AlexPn 147 6 24) LReserved "-"
, L (AlexPn 149 6 26) LLit "1"
, L (AlexPn 150 6 27) LReserved ")"
, L (AlexPn 154 7 3) LReserved "}"
, L (AlexPn 156 8 1) LReserved "}"
, L (AlexPn 159 10 1) LFnSym "@factorial"
, L (AlexPn 169 10 11) LReserved "("
, L (AlexPn 170 10 12) LLit "0"
, L (AlexPn 171 10 13) LReserved ")"
, L (AlexPn 173 11 1) LFnSym "@factorial"
, L (AlexPn 183 11 11) LReserved "("
, L (AlexPn 184 11 12) LLit "1"
, L (AlexPn 185 11 13) LReserved ")"
, L (AlexPn 187 12 1) LFnSym "@factorial"
, L (AlexPn 197 12 11) LReserved "("
, L (AlexPn 198 12 12) LLit "2"
, L (AlexPn 199 12 13) LReserved ")"
, L (AlexPn 201 13 1) LFnSym "@factorial"
, L (AlexPn 211 13 11) LReserved "("
, L (AlexPn 212 13 12) LLit "3"
, L (AlexPn 213 13 13) LReserved ")"
, L (AlexPn 215 14 1) LFnSym "@factorial"
, L (AlexPn 225 14 11) LReserved "("
, L (AlexPn 226 14 12) LLit "4"
, L (AlexPn 227 14 13) LReserved ")"
, L (AlexPn 229 15 1) LFnSym "@factorial"
, L (AlexPn 239 15 11) LReserved "("
, L (AlexPn 240 15 12) LLit "5"
, L (AlexPn 241 15 13) LReserved ")"
, L (AlexPn 243 16 1) LFnSym "@factorial"
, L (AlexPn 253 16 11) LReserved "("
, L (AlexPn 254 16 12) LLit "6"
, L (AlexPn 255 16 13) LReserved ")"
, L (AlexPn 257 17 1) LFnSym "@factorial"
, L (AlexPn 267 17 11) LReserved "("
, L (AlexPn 268 17 12) LLit "7"
, L (AlexPn 269 17 13) LReserved ")"
, L (AlexPn 271 18 1) LFnSym "@factorial"
, L (AlexPn 281 18 11) LReserved "("
, L (AlexPn 282 18 12) LLit "8"
, L (AlexPn 283 18 13) LReserved ")"
, L (AlexPn 285 19 1) LFnSym "@factorial"
, L (AlexPn 295 19 11) LReserved "("
, L (AlexPn 296 19 12) LLit "9"
, L (AlexPn 297 19 13) LReserved ")"
, L (AlexPn 299 20 1) LFnSym "@factorial"
, L (AlexPn 309 20 11) LReserved "("
, L (AlexPn 310 20 12) LLit "10"
, L (AlexPn 312 20 14) LReserved ")"
]
```

# Annex: Mutual recursion program Lexemes

```
[ L (AlexPn 34 2 1) LReserved "def"
, L (AlexPn 38 2 5) LFnSym "@isEven"
, L (AlexPn 45 2 12) LReserved "("
, L (AlexPn 46 2 13) LVarSym "$n"
, L (AlexPn 48 2 15) LReserved ")"
, L (AlexPn 50 2 17) LReserved "{"
, L (AlexPn 52 2 19) LReserved "("
, L (AlexPn 53 2 20) LReserved "if"
, L (AlexPn 56 2 23) LVarSym "$n"
, L (AlexPn 59 2 26) LReserved "=="
, L (AlexPn 62 2 29) LLit "0"
, L (AlexPn 64 2 31) LReserved "then"
, L (AlexPn 69 2 36) LLit "1"
, L (AlexPn 71 2 38) LReserved "else"
, L (AlexPn 76 2 43) LFnSym "@isOdd"
, L (AlexPn 82 2 49) LReserved "("
, L (AlexPn 83 2 50) LVarSym "$n"
, L (AlexPn 85 2 52) LReserved "-"
, L (AlexPn 86 2 53) LLit "1"
, L (AlexPn 87 2 54) LReserved ")"
, L (AlexPn 88 2 55) LReserved ")"
, L (AlexPn 90 2 57) LReserved "}"
, L (AlexPn 92 3 1) LReserved "def"
, L (AlexPn 96 3 5) LFnSym "@isOdd"
, L (AlexPn 102 3 11) LReserved "("
, L (AlexPn 103 3 12) LVarSym "$n"
, L (AlexPn 105 3 14) LReserved ")"
, L (AlexPn 107 3 16) LReserved "{"
, L (AlexPn 109 3 18) LReserved "("
, L (AlexPn 110 3 19) LReserved "if"
, L (AlexPn 113 3 22) LVarSym "$n"
, L (AlexPn 116 3 25) LReserved "=="
, L (AlexPn 119 3 28) LLit "0"
, L (AlexPn 121 3 30) LReserved "then"
, L (AlexPn 126 3 35) LLit "0"
, L (AlexPn 128 3 37) LReserved "else"
, L (AlexPn 133 3 42) LFnSym "@isEven"
, L (AlexPn 140 3 49) LReserved "("
, L (AlexPn 141 3 50) LVarSym "$n"
, L (AlexPn 143 3 52) LReserved "-"
, L (AlexPn 144 3 53) LLit "1"
, L (AlexPn 145 3 54) LReserved ")"
, L (AlexPn 146 3 55) LReserved ")"
, L (AlexPn 148 3 57) LReserved "}"
, L (AlexPn 151 5 1) LFnSym "@isEven"
, L (AlexPn 158 5 8) LReserved "("
, L (AlexPn 159 5 9) LLit "1"
, L (AlexPn 160 5 10) LReserved ")"
, L (AlexPn 162 6 1) LFnSym "@isEven"
, L (AlexPn 169 6 8) LReserved "("
, L (AlexPn 170 6 9) LLit "2"
, L (AlexPn 171 6 10) LReserved ")"
, L (AlexPn 173 7 1) LFnSym "@isEven"
, L (AlexPn 180 7 8) LReserved "("
, L (AlexPn 181 7 9) LLit "3"
, L (AlexPn 182 7 10) LReserved ")"
, L (AlexPn 184 8 1) LFnSym "@isEven"
, L (AlexPn 191 8 8) LReserved "("
, L (AlexPn 192 8 9) LLit "5"
, L (AlexPn 193 8 10) LReserved ")"
, L (AlexPn 195 9 1) LFnSym "@isEven"
, L (AlexPn 202 9 8) LReserved "("
, L (AlexPn 203 9 9) LLit "8"
, L (AlexPn 204 9 10) LReserved ")"
, L (AlexPn 206 10 1) LFnSym "@isEven"
, L (AlexPn 213 10 8) LReserved "("
, L (AlexPn 214 10 9) LLit "13"
, L (AlexPn 216 10 11) LReserved ")"
, L (AlexPn 218 11 1) LFnSym "@isEven"
, L (AlexPn 225 11 8) LReserved "("
, L (AlexPn 226 11 9) LLit "4181"
, L (AlexPn 230 11 13) LReserved ")"
, L (AlexPn 232 12 1) LFnSym "@isEven"
, L (AlexPn 239 12 8) LReserved "("
, L (AlexPn 240 12 9) LLit "6765"
, L (AlexPn 244 12 13) LReserved ")"
, L (AlexPn 246 13 1) LFnSym "@isEven"
, L (AlexPn 253 13 8) LReserved "("
, L (AlexPn 254 13 9) LLit "10946"
, L (AlexPn 259 13 14) LReserved ")"
, L (AlexPn 261 14 1) LFnSym "@isEven"
, L (AlexPn 268 14 8) LReserved "("
, L (AlexPn 269 14 9) LLit "17711"
, L (AlexPn 274 14 14) LReserved ")"
, L (AlexPn 276 15 1) LFnSym "@isEven"
, L (AlexPn 283 15 8) LReserved "("
, L (AlexPn 284 15 9) LLit "28657"
, L (AlexPn 289 15 14) LReserved ")"
, L (AlexPn 291 16 1) LFnSym "@isOdd"
, L (AlexPn 297 16 7) LReserved "("
, L (AlexPn 298 16 8) LLit "46368"
, L (AlexPn 303 16 13) LReserved ")"
]
```
