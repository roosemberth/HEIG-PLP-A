-- | This module is intended to be a fallback REPL using trivial functions
-- and without any ergonomic features (such as history or editing).

module Main where

import System.Environment (getArgs)
import System.IO (hFlush, stdout)
import System.Posix.Internals (c_isatty)
import SPL.Interpreter (Env, lexAndParse, run)

repl :: Env -> IO ()
repl env = do
  putStr "# SPL > "
  hFlush stdout
  line <- getLine
  putStrLn ""
  case lexAndParse line of
    Left err -> putStrLn ("Error: " ++ err) >> repl env
    Right spl -> case run env spl of
      Right (env', res) -> mapM_ print res >> repl env'
      Left err          -> putStrLn ("Error: " ++ err) >> repl env

main :: IO ()
main = repl []
