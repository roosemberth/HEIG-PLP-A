{-# LANGUAGE LambdaCase #-}
module Main where

import ReadLine
import System.Environment (getArgs)
import System.Posix.Internals (c_isatty)
import SPL.Interpreter (Term, Env, lexAndParse, run)

isatty :: Int -> IO Bool
isatty = fmap (0 /=) . c_isatty . fromIntegral

data SessionState = SessionState
  { historyPrev :: [String]
  -- | If not empty, the head contains the history entry currently displayed
  , historyNext :: [String]
  , inputBuffer :: String
  } deriving Show

instance Semigroup SessionState where
  (SessionState p1 n1 b1) <> (SessionState p2 n2 b2) =
    SessionState (p1 <> n1 <> p2) n2 (b1 <> b2)

instance Monoid SessionState where
  mempty = SessionState [] [] []

clearInputBuf :: SessionState -> SessionState
clearInputBuf (SessionState p n b) = SessionState p n ""

pushToHist :: String -> SessionState -> SessionState
pushToHist buf (SessionState p n ib) = SessionState (buf:(reverse n ++ p)) [] ib

pushToHist' :: String -> SessionState -> SessionState
pushToHist' b = pushToHist b . clearInputBuf

seekNextHist :: SessionState -> SessionState
seekNextHist (SessionState p [] ib) = SessionState p [] ib
seekNextHist (SessionState p [n1] _) = SessionState (n1:p) [] ""
seekNextHist (SessionState p (n1:n2:ns) _) = SessionState (n1:p) (n2:ns) n2

seekPrevHist :: SessionState -> SessionState
seekPrevHist (SessionState [] n ib) = SessionState [] n ib
seekPrevHist (SessionState (p:ps) n _) = SessionState ps (p:n) p

-- | REPL with a session state
repl :: SessionState -> Env -> IO ()
repl sst env = do
  let processLine line = case lexAndParse line of
        Left err -> do
          putStrLn ("Error: " ++ err)
          repl (pushToHist' line sst) env
        Right spl -> do
          let sst' = if null line
                        then clearInputBuf sst
                        else pushToHist' line sst
          case run env spl of
            Right (env', res) -> do
              mapM_ print res
              repl sst' env'
            Left err -> do
              putStrLn ("Error: " ++ err)
              repl sst' env
  putStr "SPL > "
  readLine (inputBuffer sst) >>= \case
    Right line -> do
      putStrLn ""
      processLine line
    Left Exit -> do
      putStrLn ""
      putStrLn "BYE!"
    Left (ClearedTerm buf) -> do
      repl (sst {inputBuffer = buf}) env
    Left (HistoryNext _) -> do
      repl (seekNextHist sst) env
    Left (HistoryPrev _) -> do
      repl (seekPrevHist sst) env

runSPLstr :: String -> Either String [Term]
runSPLstr code = lexAndParse code >>= fmap snd . run []

printExecutionResults :: Either String [Term] -> IO ()
printExecutionResults (Left err) = putStrLn $ "Error: " <> err <> "."
printExecutionResults (Right res) = mapM_ print res

main :: IO ()
main = do
  args <- getArgs

  if null args
  then do
    interactive <- isatty 0
    if interactive
    then repl mempty []
    else getContents >>= printExecutionResults . runSPLstr
  else
    readFile (head args) >>= printExecutionResults . runSPLstr
