<!-- This document may be rendered using the following commands:
pandoc l3-mai_palacios.lhs -f markdown -s -o l3-mai_palacios.pdf
pandoc l3-mai_palacios.lhs -f markdown -s -o l3-mai_palacios.html
-->
---
title: PLP-A 2020 Lab 3
author:
  - Roosembert Palacios
  - Mai Hoang Anh
---
<!-- (C) Roosembert Palacios, 2020
         Mai Hoang Anh, 2020

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
-->

This file is markdown-flavored literate haskell.
Please compile with options `-optL -q`. (e.g. `ghc -optL -q -Wall l3.lhs`.)

Boilerplate
----

Use of template haskell enables quickcheck to find all test cases.
```haskell
> {-# LANGUAGE TemplateHaskell #-}
```

Type-applying clears `-Wtype-defaults` warnings.
```haskell
> {-# LANGUAGE TypeApplications #-}
```

Various imports
```haskell
> import Control.Monad
> import Data.Bifunctor (bimap)
> import System.Exit

> -- QuickCheck >= 2
> import Test.QuickCheck
```

Util functions:

```haskell
> increasing :: Ord a => [a] -> Bool
> increasing [] = True
> increasing (a:b:_) | a > b = False
> increasing (_:l) = increasing l

> peanoEnc :: Int -> (a -> a) -> (a -> a)
> peanoEnc 0 _ = id
> peanoEnc n f = f . (peanoEnc (n-1) f)

> isSet :: Eq a => [a] -> Bool
> isSet []                   = True
> isSet (a:as) | a `elem` as = False
> isSet (_:as)               = isSet as
```

17) Facteurs premier
----

_Écrivez une fonction `factors` qui rend la liste des facteurs premiers de n.
Résolvez à l'aide d'une fonction auxiliaire `décompose n f` dans une clause
`where`._

```haskell
> factors :: Integral a => a -> [a]
> factors num = snd $ foldl conquer (num, []) [2..num]
>   where décompose n f | n `mod` f == 0 = (f:) <$> décompose (n `div` f) f
>         décompose n _ | otherwise = (n, [])
>         conquer r@(1, _) _ = r
>         conquer (toDiv, prev) factor = (prev ++) <$> décompose toDiv factor
```

Tests:

```haskell
> prop_factorsEx :: Property
> prop_factorsEx = factors @Integer 60 === [2,2,3,5]

> prop_factorsInc :: Positive Int -> Property
> prop_factorsInc (Positive n) = property $ increasing (factors n)

> prop_factors :: Positive Int -> Property
> prop_factors (Positive n) = foldr (*) 1 (factors n) === n
```

18) Types
----

_Écrivez puis vérifierz la signature de quelques fonctions que vous avez écrit
en solution aux exercises._

(Please see the corresponding function definitions.)

19) `case`
----

_Écrivez la fonction `take' n liste` en utilisant un `case` qui va permettre de
tester, par motif, les différentes variantes de `n` et de `liste`. On admet que
`n` est nul ou positif._

```haskell
> take' :: (Eq a, Num a) => a -> [b] -> [b]
> take' 0 _ = []
> take' n ls = case ls of
>   []     -> []
>   (x:xs) -> x : take' (n-1) xs
```

Tests:

```haskell
> prop_takeLength :: NonNegative Int -> [Int] -> Property
> prop_takeLength (NonNegative n) ls = length (take' n ls) === min (length ls) n

> prop_takeSublist :: NonNegative Int -> [Int] -> Property
> prop_takeSublist (NonNegative n) ls = property $ take' n ls `isPrefix` ls
>   where isPrefix [] _ = True
>         isPrefix _ [] = False
>         isPrefix (a:as) (b:bs) | a == b = isPrefix as bs
>         isPrefix _ _ = False
```

20) Récursivité : Nombres
----

_Écrire une fonction `ack (m, n)` qui calcule la fonction d'Ackermann._

```haskell
> ack :: (Ord a, Num a) => a -> a -> a
> ack 0 n                  = n + 1
> ack m 0 | m > 0          = ack (m-1) 1
> ack m n | m > 0 && n > 0 = ack (m-1) (ack m (n-1))
> ack _ _ = undefined
```

_Définir la fonction `quotient n m` qui calcule le quotient entier de `n` par
`m` sans faire appel à un opérateur de division._

```haskell
> quotient :: (Num a, Ord a, Integral b) => a -> a -> b
> quotient n m | n >= m = 1 + quotient (n-m) m
> quotient _ _ = 0
```

_Écrire une fonction `puissance x i` récursive qui calcule x^i pour i >= 0._

```haskell
> puissance :: (Num a, Eq b, Num b) => a -> b -> a
> puissance _ p | p == 0 = 1
> puissance b p = b * puissance b (p-1)
```

Tests:


```haskell
> -- | **FIXME**: Need a better test.
> prop_ack :: Property
> prop_ack = ack @Integer 1 3 === 5

> prop_quotient :: NonNegative Int -> Positive Int -> Property
> prop_quotient (NonNegative n) (Positive d) = quotient n d === div n d

> prop_puissance :: Rational -> NonNegative Int -> Property
> prop_puissance b (NonNegative p) = puissance b p === b ^^ p
```

21) Récursivité : Listes
----

_Écrire une fonction récursive `nieme n liste` qui retourne le nième élément
d'une liste (on ne se préoccupe pas des conditions d'erreur)._

```haskell
> niemeRec :: (Eq a, Num a) => a -> [b] -> b
> niemeRec 0 (a:_) = a
> niemeRec n (_:l) = niemeRec (n-1) l
> niemeRec _ _     = undefined

> niemeStd :: Int -> [b] -> b
> niemeStd = flip (!!)
```

_Écrire une fonction `duplique liste` qui duplique tous les éléments d'une
liste._

```haskell
> dupliqueRec :: [a] -> [a]
> dupliqueRec []    = []
> dupliqueRec (a:l) = a:a:dupliqueRec l

> dupliqueStd :: [a] -> [a]
> dupliqueStd l = foldr (\a r -> a:a:r) [] l
```

_Écrire une fonction `renverse liste` qui renverse les éléments dans une liste._

```haskell
> renverseRec :: [a] -> [a]
> renverseRec []    = []
> renverseRec (a:l) = renverseRec l ++ [a]  -- Very inefficient :(

> renverseStd :: [a] -> [a]
> renverseStd l = foldl (flip (:)) [] l
```

_Écrire une fonction `rotations i liste` qui réalise `i` rotations cycliques de
`liste` vers la gauche._

```haskell
> rotationRec :: Int -> [b] -> [b]
> rotationRec 0 l         = l
> rotationRec _ []        = []
> rotationRec n l | n < 0 = rotationRec (n+1) (last l : init l)
> rotationRec n l         = rotationRec (n-1) (tail l ++ [head l])

> rotationStd :: Int -> [b] -> [b]
> rotationStd _ []                = []
> rotationStd n l | -n > length l = rotationStd (n + length l) l
> rotationStd n l | n > length l  = rotationStd (n - length l) l
> rotationStd n l | n < 0         = let k = length l + n in drop k l ++ take k l
> rotationStd n l                 = drop n l ++ take n l
```

_Il y a une solution triviale avec des fonctions prédéfinies, donnez cette
solution._

_Écrivez une solution récursive._

Tests:

```haskell
> prop_niemeRec :: [Int] -> Positive Int -> [Int] -> Property
> prop_niemeRec l1 (Positive n) l2 = length l1 > n ==>
>   niemeRec (length l1) (l1++[n]++l2) === n
> prop_niemeStd :: [Int] -> Positive Int -> [Int] -> Property
> prop_niemeStd l1 (Positive n) l2 = length l1 > n ==>
>   niemeStd (length l1) (l1++[n]++l2) === n
> prop_nieme :: [Int] -> Positive Int -> Property
> prop_nieme l (Positive n) = length l > n ==> niemeStd n l === niemeRec n l

> prop_dupliqueRec :: (Eq a, Show a) => [a] -> Property
> prop_dupliqueRec ls = partage (dupliqueRec ls) === (ls, ls)
> prop_dupliqueStd :: (Eq a, Show a) => [a] -> Property
> prop_dupliqueStd ls = partage (dupliqueStd ls) === (ls, ls)
> prop_duplique :: (Eq a, Show a) => [a] -> Property
> prop_duplique ls = dupliqueStd ls === dupliqueRec ls

> prop_renverseRec :: (Eq a, Show a) => [a] -> Property
> prop_renverseRec l = renverseRec (renverseRec l) === l
> prop_renverseStd :: (Eq a, Show a) => [a] -> Property
> prop_renverseStd l = renverseStd (renverseStd l) === l
> prop_renverse :: (Eq a, Show a) => [a] -> Property
> prop_renverse l = renverseStd l === renverseRec l

> prop_rotationRecRewind :: (Eq a, Show a) => Int -> [a] -> Property
> prop_rotationRecRewind n l = rotationRec (-n) (rotationRec n l) === l
> prop_rotationRecPump :: (Eq a, Show a) => Positive Int -> [a] -> Property
> prop_rotationRecPump (Positive n) l =
>   (peanoEnc (length l) (rotationRec n)) l === l

> prop_rotationStdRewind :: (Eq a, Show a) => Int -> [a] -> Property
> prop_rotationStdRewind n l = rotationStd (-n) (rotationStd n l) === l
> prop_rotationStd :: (Eq a, Show a) => Positive Int -> [a] -> Property
> prop_rotationStd (Positive n) l =
>   (peanoEnc (length l) (rotationStd n)) l === l

> prop_rotation :: (Eq a, Show a) => Int -> [a] -> Property
> prop_rotation n l = rotationStd n l === rotationRec n l
```

22) Ensembles
----

_On utilise une liste pour modéliser un ensemble. Écrire les fonctions suivantes
de manipulation des ensembles :_

- `insertion x ensemble`: Insertion d'un élément dans un ensemble pour autant
qu'il n'existe pas encore.

```haskell
> insertion :: Eq a => a -> [a] -> [a]
> insertion a as = foldr insert' [] (a:as)
>   where insert' x l | elem x l = l
>         insert' x l            = x : l
```

- `supression x ensemble`: Supprime un élément de l'ensemble (pour autant qu'il
existe).

```haskell
> suppression :: Eq a => a -> [a] -> [a]
> suppression e ls = foldr insert' [] ls
>   where insert' x l | elem x l || x == e = l
>         insert' x l                      = x : l
```

- `union ensemble1 ensemble2`: Calcule l'union de deux ensembles, sans créer de
doublons.

```haskell
> union :: Eq a => [a] -> [a] -> [a]
> union l1 l2 = foldr insertion [] (l1 ++ l2)
```

- `intersection ensemble1 ensemble2`: Calcule l'intersection de deux ensembles.

```haskell
> intersection :: Eq a => [a] -> [a] -> [a]
> intersection l1 l2 = foldr insertion [] [a | a <- l1, b <- l2, a == b]
```

Tests:

```haskell
> prop_insertionIdemp :: (Eq a, Show a) => a -> [a] -> Property
> prop_insertionIdemp x l = peanoEnc 2 (insertion x) l === insertion x l
> prop_insertionContains :: (Eq a, Show a) => a -> [a] -> Property
> prop_insertionContains x l = property . elem x $ insertion x l
> prop_insertionSet :: (Eq a, Show a) => a -> [a] -> Property
> prop_insertionSet x l = property . isSet $ insertion x l

> prop_supressionIdemp :: (Eq a, Show a) => a -> [a] -> Property
> prop_supressionIdemp x l = peanoEnc 2 (suppression x) l === suppression x l
> prop_supressionContains :: (Eq a, Show a) => a -> [a] -> Property
> prop_supressionContains x l = property . not $ elem x (suppression x l)
> prop_supressionSet :: (Eq a, Show a) => a -> [a] -> Property
> prop_supressionSet x l = property . isSet $ suppression x l

> prop_unionIdemp :: (Eq a, Show a) => [a] -> [a] -> Property
> prop_unionIdemp l1 l2 = peanoEnc 2 (union l1) l2 === union l1 l2
> prop_unionContains :: (Eq a, Show a) => [a] -> [a] -> Property
> prop_unionContains l1 l2 = property $ all (flip elem s) (l1 ++ l2)
>   where s = union l1 l2
> prop_unionSet :: (Eq a, Show a) => [a] -> [a] -> Property
> prop_unionSet l1 l2 = property . isSet $ union l1 l2

> prop_intersectionIdemp :: (Eq a, Show a) => [a] -> [a] -> Property
> prop_intersectionIdemp l1 l2 =
>   peanoEnc 2 (intersection l1) l2 === intersection l1 l2
> prop_intersectionContains :: (Eq a, Show a) => [a] -> [a] -> Property
> prop_intersectionContains l1 l2 =
>   property $ all (\e -> elem e l1 && elem e l2) (intersection l1 l2)
> prop_intersectionSet :: (Eq a, Show a) => [a] -> [a] -> Property
> prop_intersectionSet l1 l2 = property . isSet $ intersection l1 l2
```

23) Tri par insertion
----

_Une manière triviale de trier une liste est d'appliquer la méthode de tri par
insertion : on insère successivement tous les éléments de la liste dans une
liste triée._

_Écrivez une fonction qui réalise un tri de cette manière._

```haskell
> insertionSort :: Ord a => [a] -> [a]
> insertionSort = foldr insertSorted []
>   where insertSorted e [] = [e]
>         insertSorted e l@(a:_) | e == a = l
>         insertSorted e l@(a:_) | e < a  = e : l
>         insertSorted e (a:as)           = a : insertSorted e as
```

Tests:

```haskell
> prop_insertionSortInc :: Ord a => [a] -> Property
> prop_insertionSortInc = property . increasing . insertionSort
```

24) Éléments pairs et impairs
----

_Écrivez une fonction pour filtrer (garder) les éléments de `rang` pair d'une
liste._

```haskell
> pairs :: [a] -> [a]
> pairs = snd . partage
```

Tests:

```haskell
> prop_pairs :: Eq a => [a] -> Property
> prop_pairs l = property . all id . map (uncurry (==)) $
>   zip (pairs l) [l !! (2*i - 1) | i <- [1..div (length l) 2]]
```


25) Tri fusion
----

_Écrire un fonction qui partage une liste en deux parties égales (à un élément
près)._

```haskell
> partage :: [a] -> ([a], [a])
> partage []      = ([], [])
> partage [a]     = ([a], [])
> partage (a:b:l) = bimap (a:) (b:) (partage l)
```

_Écrire une fonction qui réalise la fusion de 2 listes triées._

```haskell
> fusion :: Ord a => [a] -> [a] -> [a]
> fusion [] b = b
> fusion a [] = a
> fusion l@(a:_) (b:bs) | a > b = b : fusion l bs
> fusion (a:as) l = a : fusion as l
```

_Écrire une fonction qui réalise le tri fusion._

```haskell
> mergeSort :: Ord a => [a] -> [a]
> mergeSort []  = []
> mergeSort [a] = [a]
> mergeSort l   = uncurry fusion $ bimap mergeSort mergeSort (partage l)
```

Tests:

```haskell
> prop_partageLengths :: [Int] -> Property
> prop_partageLengths ls = let (l1, l2) = partage ls in
>   case (length ls `mod` 2) of
>    0 -> length l1 === length l2
>    1 -> length l1 === 1 + length l2
>    _ -> undefined

> prop_partageContainsElems :: [Int] -> Property
> prop_partageContainsElems ls = let (l1, l2) = partage ls in
>   case (length ls `mod` 2) of
>    0 -> foldLists l1 l2 === ls
>    1 -> foldLists (init l1) l2 ++ [last l1] === ls
>    _ -> undefined
>   where foldLists as bs = foldr (\(a, b) l -> a:b:l) [] (zip as bs)

> prop_fusion :: SortedList Int -> Property
> prop_fusion (Sorted l) = uncurry fusion (partage l) === l

> prop_mergeSortIsSorted :: [Int] -> Property
> prop_mergeSortIsSorted l = property . increasing $ mergeSort l
```

Instrumentation
----

Required so quickcheck can find the tests.

```haskell
> return []
```

Have the main function run the tests and return an adequate status code.

```haskell
> main :: IO ()
> main = $quickCheckAll >>= flip unless exitFailure
```

<!-- vim:ft=markdown
-->
